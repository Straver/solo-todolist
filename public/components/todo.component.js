angular
  .module('nodeStarterApp')
  .component('todoComponent', {
    bindings: {
      props: '<',
      onDone: '&',
      onNameChange: '&',
      onRemove: '&'
    },
    controllerAs: 'vm',
    controller: function TodoController() {
      this.isEditModeEnabled = false;

      this.delete = () => {
        this.onRemove({$event: {id: this.props._id}});
      };

      this.done = () => {
        this.onDone({$event: {id: this.props._id}});
      }

      this.edit = () => {
        this.isEditModeEnabled = true;
      };

      this.save = () => {
        this.isEditModeEnabled = false;
        this.onNameChange({$event: {id: this.props._id, text: this.props.text}});
      };
    },
    template: `
      <div class="todo" ng-class="{'done': vm.props.done}">
        <span class="text" ng-if="!vm.isEditModeEnabled">{{vm.props.text}}</span>
        <input type="text" placeholder="Todo name" ng-if="vm.isEditModeEnabled" ng-model="vm.props.text">
        <div class="todo-btns">
          <span ng-if="!vm.isEditModeEnabled">
            <button type="button" class="btn" ng-click="vm.done()" ng-if="!vm.props.done">Done</button>
            <button type="button" class="btn" ng-click="vm.edit()">Edit</button>
            <button type="button" class="btn" ng-click="vm.delete()">Remove</button>
          </span>

          <button type="button" class="btn" ng-click="vm.save()" ng-if="vm.isEditModeEnabled">Save</button>
        </div>
      </div>
    `
  });