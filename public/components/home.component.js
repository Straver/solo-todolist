angular
  .module('nodeStarterApp')
  .component('homeComponent', {
    controllerAs: 'vm',
    controller: function HomeController($http) {
      console.log('Homecomponent initialised');
      this.todos = [];

      $http.get('/api/todos')
        .then(response => {
          this.todos = response.data;
        });

      this.addTodo = (text) => {
        if (text) {
          this.text = '';
          $http.post('/api/todos', {
            done: false,
            text: text
          }).then(response => {
              this.todos = response.data;
            });
        }
      };

      this.changeName = ($event) => {
        const {id, text} = $event;
        const params = {text: $event.text};
        $http.post('/api/todos/edit/' + id, params)
          .then(response => {
            console.log(response);
            this.todos = response.data;
          });
      };

      this.setDone = ($event) => {
        const id = $event.id;
        $http.put('/api/todos/done/' + id)
          .then(response => {
            this.todos = response.data;
          });
      };

      this.removeTodo = ($event) => {
        const id = $event.id;
        $http.delete('/api/todos/' + id)
          .then(response => {
            this.todos = response.data;
          });
      };
    },
    template: `
      <div class="todo-container">
        <div class="todo-list-head">
          <input type="text" class="input" ng-model="vm.text" placeholder="Enter todo description">
          <button type="button" class="btn btn-inverted" ng-click="vm.addTodo(vm.text)">Add</button>
        </div>
        <div class="todo-list" ng-if="vm.todos && vm.todos.length">
          <todo-component
            ng-repeat="todo in vm.todos"
            props="todo"
            on-done="vm.setDone($event)"
            on-name-change="vm.changeName($event)"
            on-remove="vm.removeTodo($event)"
          ></todo-component>
        </div>
      </div>
    `
  });