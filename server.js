const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/node_starter', {
  promiseLibrary: global.Promise,
  useMongoClient: true
});

app.use(express.static(__dirname + '/public-react/public'));
app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.json());
app.use(bodyParser.json({type: 'application/vnd.api+json'}));

app.listen(8080);
console.log('App listening on port 8080');

const Todo = mongoose.model('Todo', {
  done: Boolean,
  text: String
});

app.get('/api/todos', (req, res) => {
  Todo.find({}, (err, todos) => {
    if (err) {
      res.send(err);
    }

    res.json(todos);
  });
});

app.post('/api/todos', (req, res) => {
  Todo.create({
    text: req.body.text,
    done: false
  }, (err, todo) => {
    if (err) {
      res.send(err);
    }

    Todo.find({}, (err, todos) => {
      if (err) {
        res.send(err);
      }
      res.json(todos);
    });
  });
});

app.put('/api/todos/done/:todo_id', (req, res) => {
  Todo.update({
    _id: req.params.todo_id
  }, {
    done: true
  }, (err, todo) => {
    if (err) {
      res.send(err);
    }
    
    Todo.find({}, (err, todos) => {
      if (err) {
        res.send(err);
      }
      res.json(todos);
    });
  });
});

app.post('/api/todos/edit/:todo_id', (req, res) => {
  Todo.update({
    _id: req.params.todo_id
  }, {
    text: req.body.text
  }, (err, todo) => {
    if (err) {
      res.send(err);
    }

    Todo.find({}, (err, todos) => {
      if (err) {
        res.send(err);
      }
      res.json(todos);
    });
  })
});

app.delete('/api/todos/:todo_id', (req, res) => {
  Todo.remove({
    _id: req.params.todo_id
  }, (err, todo) => {
    if (err) {
      res.send(err);
    }

    Todo.find({}, (err, todos) => {
      if (err) {
        res.send(err);
      }
      res.send(todos);
    });
  });
});

app.get('*', (req, res) => {
  res.sendFile(__dirname + '/public/index.html');
});