import * as React from 'react';

import { ITodo } from '../model/Todo';
import '../styles/Todo.css';

interface ITodoProps {
  details: ITodo;
  onDone(id: string): void;
  onRemove(id: string): void;
  onNameChange(id: string, text: string): void;
}

interface ITodoState {
  isDone: boolean;
  isEditModeEnabled: boolean;
  text: string;
}

class Todo extends React.Component<ITodoProps, ITodoState> {
  constructor(props: ITodoProps) {
    super(props);
    this.state = {
      isDone: props.details.done,
      isEditModeEnabled: false,
      text: props.details.text
    };
  }

  public done: () => void = () => {
    this.setState({isDone: true});
    this.props.onDone(this.props.details._id);
  }

  public editName: () => void = () => {
    this.setState({isEditModeEnabled: true});
  }
  
  public delete: () => void = () => {
    this.props.onRemove(this.props.details._id);
  }
  
  public handleNameChange: (event: React.FormEvent<HTMLInputElement>) => void = (event) => {
    this.setState({text: event.currentTarget.value});
  }
  
  public saveName: () => void = () => {
    const {_id, text} = this.props.details;
    this.setState({isEditModeEnabled: false});
    this.props.onNameChange(_id, text);
  }

  public render(): JSX.Element {
    return (
      <div className={"Todo " + (this.state.isDone ? 'Todo--done' : null)}>
        {this.state.isEditModeEnabled
          ? <input type="text" placeholder="Todo name" className="input" onChange={this.handleNameChange} value={this.state.text} />
          : <span className="Todo__name">{this.state.text}</span>
        }
        <div className="Todo__btns">
          {this.state.isEditModeEnabled
            ?
              <button type="button" className="btn" onClick={this.saveName}>Save</button>
            :
              <span>
                {!this.state.isDone &&
                  <button type="button" className="btn" onClick={this.done}>Done</button>
                }
                <button type="button" className="btn" onClick={this.editName}>Edit</button>
                <button type="button" className="btn" onClick={this.delete}>Remove</button>
              </span>
          }
        </div>
      </div>
    );
  }
}

export default Todo;
