import axios, { AxiosPromise, AxiosResponse } from 'axios';
import * as React from 'react';

import { ITodo } from '../model/Todo';
import '../styles/TodoList.css';
import LoadingComponent from './Loading';
import Todo from './Todo';

interface ITodoListState {
  todoName: string;
  todos: JSX.Element[];
  todosLoaded: boolean;
}

class TodoListComponent extends React.Component<{}, ITodoListState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      todoName: '',
      todos: [],
      todosLoaded: false,
    };
  }

  public componentDidMount(): void {
    this.getTodos().then((response: AxiosResponse<ITodo[]>) => {
      this.setState({
        todos: this.createTodoListElements(response.data),
        todosLoaded: true,
      })
    });
  }
  
  public handleChange: (event: React.FormEvent<HTMLInputElement>) => void = (event) => {
    this.setState({todoName: event.currentTarget.value});
  }
  
  public addTodo: () => void = () => {
    const text: string = this.state.todoName;
    if (text) {
      this.setState({todoName: ''});
      axios.post('/api/todos', {done: false, text})
        .then((response: AxiosResponse<ITodo[]>) => {
          this.setState({
            todoName: '',
              todos: this.createTodoListElements(response.data),
            })
          });
        };
    }
    
  public changeName: (id: string, text: string) => void = (id, text) => {
    const requestBody = {text};
    axios.post('/api/todos/edit/' + id, requestBody)
      .then((response: AxiosResponse<ITodo[]>) => {
        this.setState({todos: this.createTodoListElements(response.data)});
      });
  }

  public setDone: (id: string) => void = (id) => {
    axios.put('/api/todos/done/' + id)
      .then((response: AxiosResponse<ITodo[]>) => {
        this.setState({todos: this.createTodoListElements(response.data)});
      });
    }
    
  public removeTodo: (id: string) => void = (id) => {
    axios.delete('/api/todos/' + id)
      .then((response: AxiosResponse<ITodo[]>) => {
        this.setState({todos: this.createTodoListElements(response.data)});
      });
  }
  
  public render(): JSX.Element {
    return (
      <div className="TodoList">
        <div className="TodoList__head">
          <input type="text" className="input" placeholder="Enter todo description" onChange={this.handleChange} value={this.state.todoName} />
          <button type="button" className="btn btn--inverted" onClick={this.addTodo}>Add</button>
        </div>
        {this.state.todosLoaded
          ? <div className="TodoList__list">
              {this.state.todos}
            </div>
          : <LoadingComponent />
        }
      </div>
    );
  }

  private createTodoListElements(todos: ITodo[]): JSX.Element[] {
    return todos.map((todo: ITodo) => 
      <Todo details={todo}
            key={todo._id}
            onNameChange={this.changeName} 
            onDone={this.setDone}
            onRemove={this.removeTodo} />
    );
  }
  
  private getTodos(): AxiosPromise {
    return axios.get('/api/todos');
  }
}

export default TodoListComponent;