import * as React from 'react';

function LoadingComponent(): JSX.Element {
  return (
    <div className="Loading">Loading...</div>
  );
}

export default LoadingComponent;
